#!/bin/bash
# Bash Menu Script Example
[ "$UID" -eq 0 ] || exec sudo bash "$0" "$@"
echo "GIT Repository Install Script"
echo "---"
echo "1. SET Social Engineering Toolkit"
echo "2. Install LazyKali"
echo "---"
PS3='Please choose a repository to clone :'
options=("1" "2" "3" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "1")
	git clone https://github.com/trustedsec/social-engineer-toolkit/ set/
            ;;
        "2")
           bash <(wget -qO - https://lazykali.googlecode.com/files/lazykali.sh)
		exit
            ;;
        "3")
            echo "Not Implemented"
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
done
